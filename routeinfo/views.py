from django.views import View
from django.http import HttpResponse
import json
from .models import JsonExtracter, JsonCreater, dbWriter, mts_routepoint, dbFinder


# Create your views here.
class readerView(View):
   
    def get(self, request, uri):
        jobj = JsonExtracter.makeJson(uri)
        if jobj:
            reply = JsonCreater.parse(jobj)
            dbWriter.write(reply)
        return HttpResponse('ok')

    def post(self, request):
        jobj = JsonExtracter.makeJson(uri)
        if jobj:
            reply = JsonCreater.parse(jobj)
            dbWriter.write(reply)
        return HttpResponse('ok')

class queryView(View):

    def get(self, request, route):
        r = dbFinder.findRoute(route)
        return HttpResponse(r)
        
    def post(self, request, route):
        r = dbFinder.findRoute(route)
        return HttpResponse(r)
       
