from django.apps import AppConfig


class RouteinfoConfig(AppConfig):
    name = 'routeinfo'
