from django.db import models
import requests
import re
import json
from django.contrib.gis.db import models as gmodels
from django.contrib.gis.geos import Point

#from django.contrib.gis.db import gdal

class mts_route(models.Model):
    name = models.CharField(max_length=128, null=False)

class mts_routepoint(models.Model):
    name = models.CharField(max_length=128, null=False)
    order=models.IntegerField(null=True)
    route_id = models.IntegerField(null=False)
    next_point = models.IntegerField(null=True)
    prev_point = models.IntegerField(null=True)
    point = gmodels.GeometryField(null=False)

class dbFinder():
    @staticmethod
    def findRoute(route_id):
        try:
            stops = mts_routepoint.objects.filter(route_id=route_id).order_by('order')
            reply = {}
            waypoints = []
            point = {}
            index=0
            nextindex=0
            for item in stops:
                point={}
                point["order"]=item.order
                point["name"]=item.name
                point["lat"]=item.point[0]
                point["lon"]=item.point[1]
                waypoints.append(point)
            reply["stops"]=waypoints
            return json.dumps(reply, ensure_ascii=False)
        except Exception as e:
            return e

class dbWriter():
    @staticmethod
    def write(jsonstring):
        data = json.loads(jsonstring)
        if "points" in data:
            first_id = None
            last_id = None
            prev_id = None
            for index, item in enumerate(data['points']):
                try:
                    lat=item["coordinates"][0]
                    lon=item["coordinates"][1]
                    stop, created = mts_routepoint.objects.update_or_create(route_id=item['route_id'], name=item['name'],\
                         defaults={'order': index, 'prev_point': prev_id, 'next_point': None, 'point': Point(lat,lon)})
                   
                    if index!=0:
                        mts_routepoint.objects.filter(id=prev_id).update(next_point=stop.id)
                    else:
                        first_id=stop.id

                    if index==len(data['points'])-1:
                        last_id=stop.id
                        mts_routepoint.objects.filter(id=first_id).update(prev_point=stop.id)
                        stop.next_point=first_id
                        stop.save()

                    prev_id=stop.id

                except Exception as e:
                    print(e)
       

class JsonExtracter():
    @staticmethod
    def makeJson(urlstring):
        try:
            content = requests.Session().get(urlstring)
            jsonpart = re.search('{\"counters\":[\s\S]*\"meta\":{}}', content.text).group(0)
            json_object = json.loads(jsonpart)
            return json_object
        except Exception as e:
            return None

class JsonCreater():
    @staticmethod
    def parse(jobj):
        data = {}
        #Json Route part
        route = jobj["masstransitLine"]["activeThread"]["features"]

        #Route name
        route_name = jobj["masstransitLine"]["activeThread"]['properties']["ThreadMetaData"]['name']

        #Create JSON for reply
        index = 1
        data['points'] = []
        data['route_name'] = route_name
        try:
            for item in route:
                points_element = {}
                if 'properties' in item.keys():
                    lat = item["geometries"][0]["coordinates"][0]
                    lon = item['geometries'][0]['coordinates'][1]
                    points_element['route_id'] = route_name
                    points_element["index"] = index
                    points_element["name"] = item['properties']['name']
                    points_element["coordinates"] = [lat, lon]
                    data['points'].append(points_element)
                    index += 1
            return json.dumps(data, ensure_ascii=False)
        except Exception as e:
            print(e)
            return None
