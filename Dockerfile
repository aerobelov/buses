FROM python:3.7

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir /tmp/stopsreaderapp
WORKDIR /tmp/stopsreaderapp

COPY ./requirements.txt  /tmp/stopsreaderapp/requirements.txt
RUN pip install --no-cache-dir -r requirements.txt
COPY .  /tmp/stopsreaderapp/




