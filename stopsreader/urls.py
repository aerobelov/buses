from django.urls import path
from routeinfo import views as rview

urlpatterns = [
    path('url/<path:uri>/', rview.readerView.as_view(), name='reader'),
    path('route/<str:route>/', rview.queryView.as_view(), name='query'),
]
